package main

import (
	"os"
	"testing"
)

// Testing functions shoukd start with capital letters
func TestNewCard(t *testing.T) {
	cards := cardsCreation()
	if len(cards) != 16 {
		// error formating
		t.Errorf("Expected len 16, got %v", len(cards))
	}

}

func TestSaveToFileCardFromFiles(t *testing.T) {
	os.Remove("_decktesting")

	cards := cardsCreation()
	cards.saveToFile("_decktesting")

	loadedCards := cardFromFiles("add_Cards.txt")

	if len(loadedCards) != 16 {
		t.Errorf("Expected 16, got %v", len(loadedCards))
	}
	os.Remove("_decktesting")
}
