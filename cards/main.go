package main

import "fmt"

func main() {
	// var is variable
	// card is name of variable
	// string is the type
	// right side is the value assigned
	// All types:
	// bool, string, int and float64
	// var card string = "Ace of Spades" or card := "Ace of Spades"
	// or
	cards := cardsCreation()
	cards.printFun()
	fmt.Println("'--=------=-=-=--=---=-=-=-=-=-=-=-'")
	hand, rest := deal(cards, 5)
	hand.printFun()
	fmt.Println("'----------------------'")
	rest.printFun()
	// := to initialize
	fmt.Println("'----------------------'")
	fmt.Println(hand.toStringCon())
	cards.saveToFile("add_Cards.txt")
	fmt.Println("'----------------------'")
	fmt.Println(cardFromFiles("add_Cards.txt"))
	// cards.saveToFile("add_Cards.txt")
	fmt.Println("'----------------------'")
	cards.shuffle()
	cards.printFun()

}

// func newCard() string {
// 	str := "Five of Diamonds"
// 	return str
// }
