package main

import (
	"fmt"
	"net/http"
	"time"
)

func main() {
	links := []string{
		"http://google.com",
		"https://www.bettereco.com/",
		"https://epaper.hindustantimes.com/",
		// "https://www.bettereco.dee/",
		"https://www.prezotech.com/",
		"https://www.pdf.com",
		"https://www.bettereco.de/",
		// "https://www.bettereco.ccom/",
		"https://www.youtube.com/",
		"https://www.instagram.com/",
		"https://www.facebook.com/",
		"https://market.bettereco.com/",
		"https://dev.nonprod.bettereco.io/",
	}

	channel := make(chan string)

	for _, link := range links {
		go checkLink(link, channel)
		// fmt.Println(<-channel)
	}

	// for i := 0; i < len(links); i++ {
	// 	fmt.Println(<-channel)
	// }

	// for {
	// 	go checkLink(<-channel, channel)
	// }

	// for link := range channel {
	// 	go checkLink(link, channel)
	// }

	for link := range channel {
		go func(lk string) {
			time.Sleep(12 * time.Second)
			go checkLink(lk, channel)
		}(link)
	}

}

func checkLink(link string, c chan string) {
	_, err := http.Get(link)
	if err != nil {
		fmt.Println(link, " down!!!")
		c <- link
		return
	}
	fmt.Println(link, "working`  ('_')  `")
	c <- link
}
