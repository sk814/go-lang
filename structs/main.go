package main

import "fmt"

// import "golang.org/x/text/number"

type person struct {
	name    string
	age     int
	address string
	// phoneNumber phoneNumber
	phoneNumber
}

type phoneNumber struct {
	countryCode string
	number      int
}

func main() {
	p1 := person{name: "sanjeev kumar", age: 26, address: "419, uberan club, Tegal, Berlin"}
	fmt.Println(p1)
	p1.name = "Sanjeev Kumar"
	fmt.Println(p1)

	p1 = person{
		name:    "sanjeev kumar",
		age:     26,
		address: "419, uberan club, Tegal, Berlin",
		phoneNumber: phoneNumber{
			"+49",
			17688128863,
		},
	}

	fmt.Println(p1)
	// & -> memory address
	pointerP := &p1
	fmt.Printf("address: %p, %p", &p1, pointerP)
	pointerP.updateAddress("Lux")
	fmt.Println(p1)
	//Shortcut for call by value (with * as type in funcation, go will automaticaly convert into the pointer)
	p1.updateAddress("Luxemb")
	fmt.Println(p1)
	fmt.Printf("address: %p, ", pointerP)

	testSlice := []int{1, 2, 3, 4, 0, 0, 5}
	fmt.Println(len(testSlice))

}

// pass by value
func (p *person) updateAddress(newAddress string) {
	// * -> value at the address
	(*p).address = newAddress
	fmt.Printf("address2: %p, ", &p)
}
