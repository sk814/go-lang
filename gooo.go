// package should be first line
// Executable package: created to do some task
// reusable packages: reuse some code (like main) - libraries
// main package is for compilation and to run the code
// Consider packges as a folder which has multiple files (classes)
package main

// sort form of format
// for print, logging
// import "fmt" to link it to the main
// fmt is the package linked to main
import "fmt"

// func keyword
// main -> name
// () -> for parameter
// {} -> for function body
func main() {
	fmt.Print("first code in go")
}
