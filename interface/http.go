package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
)

type logWriter struct{}

func httpCall(site string) {
	res, err := http.Get(site)
	if err != nil {
		fmt.Println("Error!!!:", err)
		os.Exit(1)
	}
	fmt.Println(res.Body)
	responseByte := make([]byte, 99999)
	res.Body.Read(responseByte)
	// fmt.Println(responseByte)
	// fmt.Println(string(responseByte))
	fmt.Println("----")
	// To implement the writer interface (which has Write function) inderectly
	lw := logWriter{}
	// io.Copy(os.Stdout, res.Body)
	io.Copy(lw, res.Body)

}

func (logWriter) Write(bs []byte) (int, error) {
	fmt.Println(string(bs))
	fmt.Println("------------------ BYTE LEN -----------", len(bs))
	// fmt.Println()
	return len(bs), nil
}
