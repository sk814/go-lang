package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"
)

// create a new type ['deck']
type deck []string //-> Slice of string

// receiver function
// func (d deck) printFun {} -> any variable of type deck can get access of printFun
func (d deck) printFun() {
	for i, card := range d {
		fmt.Println(i, card)
	}
}

// _ (underscore) is used when the value is not usefull
func cardsCreation() deck {
	cards := deck{}
	cardSuits := []string{"Space", "Diamond", "Hearts", "Clubs "}
	cardvalues := []string{"Ace", "One", "Two", "Three"}

	for _, suit := range cardSuits {
		for _, val := range cardvalues {
			cards = append(cards, val+" of "+suit)
		}
	}

	for i := 0; i < 10; i++ {
		if i%2 == 0 {
			fmt.Printf("%v is even\n", i)
		} else {
			fmt.Printf("%v is odd\n", i)
		}
	}

	return cards
	// lastNumber := 10
	// type nums []int
}

func (d deck) toStringCon() string {
	return strings.Join(d, ",")
}

func deal(d deck, size int) (deck, deck) {
	return d[:size], d[size:]
}

func (d deck) saveToFile(filename string) error {
	return ioutil.WriteFile(filename, []byte(d.toStringCon()), 0666)
}

func cardFromFiles(filename string) deck {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println("err:", err)
		//code zero indicate success
		os.Exit(1)
	}
	return deck(strings.Split(string(data), ","))
	// string(data)
}

func (d deck) shuffle() {
	fmt.Println(time.Now().UnixNano())
	fmt.Println(time.Now())
	seed := rand.NewSource(time.Now().UnixNano())
	r := rand.New(seed)
	for i := range d {
		// newPos := rand.Intn(len(d) - 1)
		newPos := r.Intn(len(d) - 1)
		d[i], d[newPos] = d[newPos], d[i]
	}
}
