package main

import (
	"fmt"
)

// automatically links are created with interface for eg: engBot and germanBot are now type of bot as well or included in bot.
//What is interface in go?
// Interface defins the behavior for similar type of objects.
type bot interface {
	getGreeting() string
}

type engBot struct{}
type germanBot struct{}

func main() {
	httpCall("https://google.com")
	eb := engBot{}
	gb := germanBot{}

	greet(eb)
	greet(gb)
}

func greet(b bot) {
	fmt.Println(b.getGreeting())
	// fmt
}

func (engBot) getGreeting() string {
	return "Hi!"
}

func (germanBot) getGreeting() string {
	return "Hallo!"
}
