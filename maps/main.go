package main

import "fmt"

func main() {
	colors := map[string]string{
		"white": "fffffff",
		"black": "00000",
	}
	fmt.Println(colors)

	// type of declaring map
	var colors2 map[string]string

	colors3 := make(map[string]string)

	fmt.Println(colors2, colors3)

	colors["offwhite"] = "ffff0"

	fmt.Println(colors)
	loopIter(colors)

}

func loopIter(m map[string]string) {
	for key, val := range m {
		fmt.Println(key, val)
	}
}
